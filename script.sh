#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Error: Script requires one argument, the name of the directory to search."
    exit 1
fi

if [ ! -d "$1" ]; then
    echo "Error: $1 is not a directory."
    exit 1
fi

files=$(find "$1" -type f -exec stat -f "%N,%z,%Sm" {} + | while read line; do 
    file_name=$(printf '%q\n' "$line" | awk -F, '{print $1}'); 
    eval file_name=${file_name}
    sha256=$(shasum -a 256 "$file_name" | awk '{print $1}');
    echo "$line,$sha256"; 
done)

header="File,Size,Creation_Date,Checksum"

echo -e "$header\n$files" > "$1_file_list.csv"
